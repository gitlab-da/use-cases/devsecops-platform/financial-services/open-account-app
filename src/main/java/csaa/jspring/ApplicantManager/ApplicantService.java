package csaa.jspring.ApplicantManager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicantService {

   @Autowired
   private ApplicantRepository clientRepo;
    
   public ApplicantService() {
    super();
   }

   public List<Applicant> findAll() {
    return this.clientRepo.findAll();
   }

   public void add(final Applicant applicant) {
    this.clientRepo.add(applicant);
   }
}
