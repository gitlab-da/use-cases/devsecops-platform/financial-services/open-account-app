package csaa.jspring.ApplicantManager;

public class Applicant {
	private Type type;
	private String firstName;
    private String lastName;
	private String email;
    private String driversLicense;
	private String stateOfIssuance;
    private String employer;
	private String income;
    
	public Applicant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Applicant(Type type, String firstName, String lastName, String email, String driversLicense, String stateOfIssuance,
			String employer, String income) {
		this.type = Type.CHECKING;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.driversLicense = driversLicense;
		this.stateOfIssuance = stateOfIssuance;
		this.employer = employer;
		this.income = income;
	}

	public Type getType() {
		return type;
	}

	public void setType(final Type type) {
		this.type =type;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getDriversLicense() {
		return driversLicense;
	}

	public void setDriversLicense(final String driversLicense) {
		this.driversLicense = driversLicense;
	}

	public String getStateOfIssuance() {
		return stateOfIssuance;
	}

	public void setStateOfIssuance(final String stateOfIssuance) {
		this.stateOfIssuance = stateOfIssuance;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(final String employer) {
		this.employer = employer;
	}

	public String getIncome() {
		return income;
	}

	public void setIncome(final String income) {
		this.income = income;
	}
}
