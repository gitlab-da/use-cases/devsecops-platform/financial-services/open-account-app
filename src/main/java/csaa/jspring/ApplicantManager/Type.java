package csaa.jspring.ApplicantManager;

public enum Type {
    SAVINGS("SAVINGS"),
    CHECKING("CHECKING");

    public static final Type[] ALL = { SAVINGS, CHECKING };

    private final String name;

    public static Type forName(final String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null for type");
        }
        if (name.toUpperCase().equals("SAVINGS")) {
            return SAVINGS;
        } else if (name.toUpperCase().equals("CHECKING")) {
            return CHECKING;
        }
        throw new IllegalArgumentException("Name \"" + name + "\" does not correspond to any Type");
    }
    
    
    private Type(final String name) {
        this.name = name;
    }
    
    
    public String getName() {
        return this.name;
    }
    
    @Override
    public String toString() {
        return getName();
    }
}
