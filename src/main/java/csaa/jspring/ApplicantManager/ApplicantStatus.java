package csaa.jspring.ApplicantManager;

public class ApplicantStatus {
    private String firstName;
    private String lastName;
    private String status;
    private int creditScore;
    private boolean applicantApproved;

    public ApplicantStatus() {
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public int getCreditScore() {
        return creditScore;
    }
    public void setCreditScore(int creditScore) {
        this.creditScore = creditScore;
    }
    public boolean isApplicantApproved() {
        return applicantApproved;
    }
    public void setApplicantApproved(boolean applicantApproved) {
        this.applicantApproved = evaluateApplicantApproval(creditScore);
    }

/**
 * Evaluates whether an applicant should be approved or not based on their credit score.
 *
 * @param creditScore The credit score of the applicant.
 * @return true if the applicant should be approved, false otherwise.
 */
private boolean evaluateApplicantApproval(int creditScore) {
    // if creditScore is exceptional then approve applicant, else if creditScore is very good then approve applicant,
    // else if creditScore is good then approve applicant. If creditScore is fail or poor then do not approve applicant
    if (creditScore >= 800) {
        return true;
    } else if (creditScore >= 700) {
        return true;
    } else if (creditScore >= 600) {
        return true;
    } else {
        return false;
    }
}
}