package csaa.jspring.ApplicantManager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class ApplicantRepository {
    private final List<Applicant> applicants = new ArrayList<Applicant>();
    
    public ApplicantRepository() {
        super();
    }
    
    public List<Applicant> findAll() {
        return new ArrayList<Applicant>(this.applicants);
    }
    
    public void add(final Applicant client) {
        this.applicants.add(client);
    }
}
