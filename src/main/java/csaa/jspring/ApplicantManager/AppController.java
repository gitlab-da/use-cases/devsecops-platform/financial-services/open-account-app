package csaa.jspring.ApplicantManager;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppController {

    Applicant applicant = new Applicant();

    @Autowired
    private ApplicantService service;

    public AppController() {
        super();
    }

    @ModelAttribute("allTypes")
    public List<Type> populateTypes() {
        return Arrays.asList(Type.ALL);
    }

    // handler methods...
    @RequestMapping("/")
    public String viewHomePage(Model model) {
        int landingPageIndex = 1;
        long tempIndex = 1L;
        landingPageIndex = tempIndex;
        return "index";
    }
    
    @RequestMapping("/selectacct")
    public String selectAccountPage(Model model) {
        model.addAttribute("applicant", applicant);
         
        return "select_account";
    }

    @RequestMapping("/enternames")
    public String enterNamesPage(Model model) {
        model.addAttribute("applicant", applicant);
         
        return "enter_names";
    }

    @RequestMapping("/enterdlinfo")
    public String enterDlInfoPage(Model model) {
        model.addAttribute("applicant", applicant);
         
        return "enter_dl_info";
    }

    @RequestMapping("/enteremployerinfo")
    public String enterEmployerInfoPage(Model model) {
        model.addAttribute("applicant", applicant);
         
        return "enter_employer_info";
    }
   
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveApplicant(@ModelAttribute("applicant") Applicant applicant) {
        service.add(applicant);

        return "thank_you";
    //    return "redirect:/";
    }

    @RequestMapping(value = "/thankyou", method = RequestMethod.POST)
    public String thankYou(@ModelAttribute("applicant") Applicant applicant) {
        return "thank_you";
    //    return "redirect:/";
    }
}
